/*
 * This module handles the dispatching of menu reactions
 * In some cases we may need to tie logic to some callback
 * Those callbacks can be tied up here
 */
import { CommandInteraction, MessageActionRow, SelectMenuInteraction } from 'discord.js'
import { help_command } from './commands'
import { toggle_server } from './server'


export const SERVER_TOGGLER = 'server-toggler'
export const SERVER_IP_GRAB = 'server-ip-grab'

export async function dispatch(interaction: CommandInteraction | SelectMenuInteraction, menu_id: string, values: unknown) {
	switch (menu_id) {
		case SERVER_TOGGLER : {
			const response = await toggle_server(values[0])
			await interaction.editReply({
				content: 'This may take a minute',
				components: [],
				embeds: [response]
			})
			break;
		}
		default : await interaction.editReply({ embeds : [help_command()]})
	}
}
