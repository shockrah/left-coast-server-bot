import got from 'got'
import * as color from './colors'
import { stderr } from './log'
import { MessageEmbed } from 'discord.js'

const tags: Array<string> = [
    '3d',
    'ahegao',
    'anal',
    'bdsm',
    'big boobs',
    'blow job',
    'bondage',
    'boob job',
    'censored',
    'comedy',
    'cosplay',
    'creampie',
    'dark skin',
    'facial',
    'fantasy',
    'filmed',
    'foot job',
    'futanari',
    'gangbang',
    'glasses',
    'hand job',
    'harem',
    'hd',
    'horror',
    'incest',
    'inflation',
    'lactation',
    'loli',
    'maid',
    'masturbation',
    'milf',
    'mind break',
    'mind control',
    'monster',
    'nekomimi',
    'ntr',
    'nurse',
    'orgy',
    'plot',
    'pov',
    'pregnant',
    'public sex',
    'rape',
    'reverse rape',
    'rimjob',
    'scat',
    'school girl',
    'shota',
    'softcore',
    'swimsuit',
    'teacher',
    'tentacle',
    'threesome',
    'toys',
    'trap',
    'tsundere',
    'ugly bastard',
    'uncensored',
    'vanilla',
    'virgin',
    'watersports',
    'x-ray',
    'yaoi',
    'yuri'
]

class SearchItem {
    name: string
    slug: string
    // can't remember which one of these is actually correct so we'll just go with this for now
    id: string|number 
    views: number
    tags: Array<string>
    description: string
	brand: string
	cover_url: string
	monthly_rank: number
	link: string

    constructor(data: unknown) {
        this.name = data['name']
        this.slug = data['slug']
        this.id = data['id']
        this.views = data['views']
        this.tags = data['tags']
        this.description = data['description']
		this.brand = data['brand']
		this.cover_url = data['cover_url']
		this.monthly_rank = data['monthly_rank']

		this.link = 'https://hanime.tv/videos/hentai/' + this.slug
    }
}

class SearchRequest {
    search_text: string
    tags: Array<string>
    tags_mode: string
    brands: Array<string>
    blacklist: Array<string>
    order_by: string
    ordering: string
    page: number

    constructor(search_text: string|null, tags?: Array<string>) {
        this.tags_mode = 'AND'
		this.tags = (tags) ? tags : [] // basic default

        this.brands = []
        this.blacklist = []

        this.order_by = 'created_at_unix'
        this.ordering = 'desc'
        this.page = 0
		this.search_text = search_text
    }

    parse_hits(raw: string) : unknown|null {
        try {
            raw = raw.replace(/(\n+)/g, '')
            return JSON.parse(raw)
        } catch(err) {
            stderr('hanime-conversion', 'Unable to parse Hanime JSON response')
            return null
        }
    }

    async get() : Promise<Array<SearchItem>> {
		/*
		 * GENERAL JSON STRCUTURE
		 * {
		 * page: Number,
		 * nbPages: Number,
		 * hitsPerPage: Number:
		 * hits: [
		 * 	{
		 * 		name: String,
		 * 		slug: String,
		 * 		id: String,
		 * 		views: Number,
		 * 		tags: tags,
		 * 		description: String,
		 * 	}...
		 * ]
		 * }
		 */
        const base_url = 'https://search.htv-services.com/'
        const response = await got.post(base_url, {
            http2: true,
            json: this,
			headers: {
				'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36'
			}
        })
        const full_data = this.parse_hits(response.body) // make the whole body into an Object
        full_data['hits'] = JSON.parse(full_data['hits']) // then we can parse the hits array

		const results: Array<SearchItem> = []
        for(const idx in full_data['hits']) {
            try {
                results.push(new SearchItem(full_data['hits'][idx]))
            } catch(err) {
                /* attempt to mvoe on hoping its just one item thats fucked */
                continue
            }
        }
        return results
    }
}


export async function search_via_tags(raw: Array<string>) : Promise<MessageEmbed> {
    let valid_tags = true;
    for(const tag of raw) {
        if(tags.indexOf(tag) == -1) {
            valid_tags = false;
            break
        }
    }
    if(valid_tags == false) {
        return new MessageEmbed()
            .setTitle('Invalid tags')
            .setColor(color.ERROR)
            .setDescription('Check `.htags` for a full list of valid tags')
    }
    try {
        const request = new SearchRequest('', raw)
        const page = await request.get()

        const random_post_index = Math.floor(Math.random() * page.length)
        const post = page[random_post_index]

		return new MessageEmbed()
			.setTitle(post.name)
			.setDescription(post.description.substr(0,250))
			.setColor(0xffffff)
			.setAuthor({name: post.brand})
			.setImage(post.cover_url)
			.addField('Link', `[Link](${post.link})`, true)
    } catch (httperr) {
        stderr('hanime', httperr)
		stderr('hanime-err', httperr.response.status)

        return new MessageEmbed()
            .setTitle('Search Error')
            .setColor(color.ERROR)
            .setDescription('Error reports here are largely useless, check logs')
    }
}

export async function text_search(args: Array<string>) : Promise<MessageEmbed> {
	// Concat all terms into one string
	if(args.length == 0) {
        return new MessageEmbed()
            .setTitle('No text given')
            .setColor(color.ERROR)
            .setDescription('Empty searches are not valid!')
	}
	let s = ''
	args.forEach(part => s += part + ' ');
	try {
		const request  = new SearchRequest(s, [])
		const page = await request.get()

        const random_post_index = Math.floor(Math.random() * page.length)
        const post = page[random_post_index]

		return new MessageEmbed()
			.setTitle(post.name)
			.setColor(0xffffff)
			.setDescription(post.description.substr(0,250))
			.setAuthor(post.brand)
			.setImage(post.cover_url)
			.addField('Link', `[Link](${post.link})`, true)

	} catch (httperr) {
		stderr('hanime-api-failure', httperr)
        return new MessageEmbed()
            .setTitle('Search Error')
            .setColor(color.ERROR)
            .setDescription('Error logs are useless here just tell shock its busted')
	}
}

export function htags() : MessageEmbed {
    let body = ''
    for(const tag of tags) {
        body += `\`${tag}\`,  `
    }
	return new MessageEmbed()
		.setTitle("Available tags")
		.setFooter('Seperate tags by wrapping them in `"`(quotation) marks')
		.setDescription(body)



}
