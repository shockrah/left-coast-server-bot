export function now() : string {
    return new Date().toLocaleDateString('en-US', { 
        timeZone: 'America/Los_Angeles',
        hour: '2-digit',
        minute: '2-digit',
        hour12: false
    })
}

export function stdout(type: string, data: unknown) : void {
	try {
		console.log(`lewd-lad, std ${type}, ${now()}, `, data.toString())
	} catch(_err) {
		console.log(`lewd-lad, std ${type}, ${now()}, `, data)
	}
}

export function stderr(type: string, data: unknown) : void {
	const time = now()
	try {
		console.error(`lewd-lad, err ${type}, ${time}, `, data.toString())
	} catch(_err) {
		console.error(`lewd-lad, err ${type}, ${time}, `, data)
	}
}
