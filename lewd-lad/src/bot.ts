import { Client, Intents, CommandInteraction, SelectMenuInteraction } from 'discord.js'
import {stderr, stdout} from './log'
import { dispatch } from './commands'
import * as menu from './menu'
import * as replies from './replies'
import { CustomConfig } from './config'
import * as slash from './slash'

const config_path = (process.env['DEBUG']) ? '../config/test-keys.json' : null
const keys = new CustomConfig(config_path)


const client = new Client({ intents: [Intents.FLAGS.GUILDS] })

client.on('ready', async () => {
	stdout('discord-login', client.user.tag)
	// Register slash commands for all discord's
	client.guilds.cache.forEach(async (guild, key) => {
		stdout('info', 'Register commands for ' + key)
		await slash.register(client, keys.discord_token, guild)
	})
})

client.on('guildCreate', async guild => {
	await slash.register(client, keys.discord_token, guild)
	stdout('info', `Joined guild-id: ${guild.name} ${guild.id}`)
})

client.on('guildeDelete', guild => {
	stdout('info', `Removed from guild: ${guild.name} ${guild.id}`)
})

// Next we set some listener binds
client.on('interactionCreate', async (interaction: CommandInteraction|SelectMenuInteraction) => {
	// Wrappig in try to avoid crashing on bad input/processing/output
	try {
		// Figure out how we're supposed to send those things
		const action = replies.get_action(interaction, interaction.isSelectMenu())
		await replies.smartdefer(interaction, action)
		// First we generate a reply that we want to send
		const reply = await dispatch(interaction as CommandInteraction)

		if(interaction.isSelectMenu()) {
			await menu.dispatch(interaction, interaction.customId, interaction.values)
		}
		else if(interaction.isCommand()) {
			if(reply.menu) {
				await replies.menu(interaction, reply)
			} else {
				await replies.embed(interaction, reply)
			}
		}
		else {
			stdout('Info/Warn', 'Interaction type unkown')
			await replies.default_help(interaction)
		}
	} catch(err) {
		stderr('interaction-failure', err)
	}

})

client.login(keys.discord_token)
.then(() => {
	stdout('info', 'Logged in successfully')
})
.catch(reason => stderr('login-error', reason))
