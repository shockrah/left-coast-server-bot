import { stdout, stderr } from './log'
import { Client, Guild } from 'discord.js'
import { commands } from './commands'

const { REST } = require('@discordjs/rest')
const { Routes } = require('discord-api-types/v9')


export async function register(client: Client, token: string, guild?: Guild) {
	/*
	 * This function basically goes through all the available commands and
	 * registers them in the configured server(which in this case for now)
	 * is just the test realm for poc
	 */
	const id = client.user.id
	const rest = new REST({version: '9'}).setToken(token)

	const to_register = []
	for(const command of Object.keys(commands)) {
		stdout('info', `Registering ${command}`)
		to_register.push(commands[command])
	}
	const reg = async (guild_id, items) => {
		await rest.put(
			Routes.applicationGuildCommands(id, `${guild_id}`),
			{ body: items }
		)
	}
	try {
		// Take the newly passed in guild_id or default to the test realm
		if(guild) { await reg(guild.id, to_register) } 
		else { await reg('684350632580218961', to_register) }
	} catch (err) {
		stderr('ERROR', err)
	}
}

