import got  from 'got'
import { MessageEmbed, MessageActionRow, MessageSelectMenu } from 'discord.js'
import { SERVER_IP_GRAB, SERVER_TOGGLER } from './menu'
import * as color from './colors'
import { stderr } from './log'
import { CustomConfig } from './config'

const _config_path = (process.env['DEBUG']) ? '../config/test-keys.json' : null
const config = new CustomConfig(_config_path)

interface Ec2Basics {
	id: string
	state: string
	connection_str: string
}

class Instance {
	name: string|null
	id: string
	state: string
	description: string|null
	json: unknown|null
	tags: Map<string, string>
	ip: string
	connection_str: string|null

	constructor(raw: unknown) {
		// Set some default values for the instance
		this.id = raw['InstanceId']
		this.state = raw['State'] ? raw['State']['Name'] : 'Unkown'
		this.description = null
		this.name = null
		this.json = raw
		this.tags = new Map<string, string>()
		this.ip = raw['PublicIpAddress'] ? raw['PublicIpAddress'] : 'No IP :('
		this.connection_str = null

		const tags = (raw['Tags']) ? raw['Tags'] : []

		// Tag collection
		// Name + Description are special tags as they pertain to
		for(const tag of tags) {
			const key = tag['Key']
			const value = tag['Value']
			this.tags.set(key, value)

			if(key == 'Name') {
				this.name = value
			}
			if(key == 'Description' || key == 'description') {
				this.description = value
			}
			if(key == 'connection_string') {
				this.connection_str = value
			}
		}
	}
}

function build_url(endpoint: string) : string { 
	// The api we are trying to acess
	const API_ID = config.aws_api_id
	// The region where the api is ran from
	const REGION = config.aws_api_region
	// Target lambda stage
	const STAGE = config.aws_api_stage
	return `https://${API_ID}.execute-api.${REGION}.amazonaws.com/${STAGE}/${endpoint}`
}


export async function fetch_instances() : Promise<Array<Instance>> {
	// Here we're simply generating an array of instances on sucess
	const url = build_url('list')
	const response = await got(url, {
		headers: {
			'x-api-key': config.aws_api_key
		},
		responseType: 'json'
	});

	// Once we have our response we have to parse out the json we need
	// Target instance data is found under ['Reservations']['Instances']

	const instances: Array<Instance> = []
	// Each 'user'(i think) has a reservation but we want the id's of all instances
	// NOTE: in theory this could fail horribly but its ok since error handling
	// the main errors that bubble up here come from bad permissions on Lambda's end
	// and the occasional "look at that thing" error
	for(const res of response.body['Reservations']) {
		for(const i of res['Instances']) {
			instances.push(new Instance(i))
		}
	}
	return instances.filter(inst => inst.id != config.beehive_id)
}

export async function list_instances(filter_tag?: string) : Promise<MessageEmbed|MessageActionRow>  {
	try {
		let instances = await fetch_instances()
		if(filter_tag) {
			instances = instances.filter(i => i.tags.has(filter_tag))
		}
		const inst_options = []
		for(const i of instances) {
			// Useful description for more comprehensive UX
			const desc: string = i.description || 'No Description'

			inst_options.push({
				label: i.name,
				description: desc + '-' + i.state,
				// This value gets passed arround quite a lot tbh
				value: JSON.stringify({
					id: i.id,
					state: i.state,
					connection_str: i.connection_str || i.ip
				})
			})
		}
		// Return this in case there are no servers whitelisted for the guild
		if(inst_options.length == 0) {
			const dummy_menu = new MessageSelectMenu()
				.setCustomId('dummy')
				.setPlaceholder('No servers whiltelisted for this guild')
				.addOptions({
					label: 'None',
					value: 'None'
				})
			return new MessageActionRow().addComponents(dummy_menu)
		}

		// Normally we just give back the actual server list
		return new MessageActionRow()
			.addComponents(
				new MessageSelectMenu()
					.setCustomId(SERVER_TOGGLER)
					.setPlaceholder('Select server to turn on|off')
					.addOptions(inst_options)
			)

	} catch (err) {
		console.error('aws-list-instances-err', err)
		console.error('aws-list-instances-stack', err.stack)
		console.error('aws-list-instances-options', err.options)
		return new MessageEmbed()
			.setTitle('Unkown failure state')
			.setColor(color.ERROR)
			.setDescription('Someone @shockrah to fix it again')
	}
}


async function start_instance(server: Ec2Basics) : Promise<MessageEmbed> {
	// WARN: Some input sanitization is probably required here,
	// maybe some kind of regex check can be done to avoid bs situations
	try {
		const url = build_url('start')
		const response = await got(url, {
			headers: {
				'x-api-key': config.aws_api_key
			},
			searchParams: {
				'instance-id': server.id
			},
			method: 'POST',
			responseType: 'json'
		})
		const aws_response = response.body

		// Normally we don't get anything back from the API
		if(aws_response) {
			stderr('aws-start-instance', aws_response)

			return new MessageEmbed()
				.setTitle('Issue starting server')
				.setColor(color.ERROR)
				.setDescription('Fucking bezos broke something again')
		} else {
			const header = `Connect with: \`${server.connection_str}\``
			return new MessageEmbed()
				.setTitle('Server starting up!')
				.setDescription(header)
				.setColor(color.SUCESS)
		}

	} catch(err) {
		// WARN: we may have to do some kind of http error checking here
		// its totally possible that gotjs just fucks us hard here
		stderr('aws-start-instance', err)
		stderr('aws-start-instance', err.stack)
		return new MessageEmbed()
			.setTitle('Unkown failure state???')
			.setColor(color.ERROR)
			.setDescription('@shockrah or bezos to fix this')
	}
}




async function stop_instance(serv_id: string) : Promise<MessageEmbed> {
	try {
		const url = build_url('stop')
		await got(url, {
			headers: {
				'x-api-key': config.aws_api_key
			},
			searchParams: {
				'instance-id': serv_id
			},
			method: 'POST',
			responseType: 'json'
		})
		return new MessageEmbed()
			.setTitle('Stopping instance!')
			.setColor(color.SUCESS)
			.setDescription('All good')
	} catch (err) {
		stderr('aws-stop-instance-err', err)
		stderr('aws-stop-instance-stack', err.stack)
		return new MessageEmbed()
			.setTitle('AWS is fucked again')
			.setColor(color.ERROR)
			.setDescription('go yell at shockrah or bezos idk')
	}
}

export async function toggle_server(raw: string) : Promise<MessageEmbed> {
	const ec2: Ec2Basics = JSON.parse(raw)
	if(ec2.state == 'running') { 
		return await stop_instance(ec2.id) 
	}
	else {
		return await start_instance(ec2)
	}
}
