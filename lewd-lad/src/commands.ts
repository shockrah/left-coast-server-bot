const {
	SlashCommandBuilder,
	SlashCommand,
	MessageActionRow,
	Interaction
} = require('@discordjs/builders');

import { MessageEmbed, CommandInteraction } from 'discord.js'
import { list_instances } from './server'
import  { top_thread, porn_roulette } from './chan'
import { text_search, htags } from './hanime'
import * as color from './colors'
import { CustomConfig } from './config';

const _config_path = (process.env['DEBUG']) ? '../config/test-keys.json' : null
const config = new CustomConfig(_config_path)

export enum Response {
	Dm, // direct message
	OpenReply, // standard reply that anyone can view
	EphemeralReply, // reply in public chat viewable only by the recipient
}

export class CommandResponse {
	content: MessageEmbed|typeof MessageActionRow
	response_type: Response
	menu: boolean

	constructor(content: typeof MessageActionRow|MessageEmbed, response: Response, is_menu?: boolean) {
		this.content = content
		this.response_type = response
		this.menu = is_menu
	}
}


export function is_ephemeral(interaction: typeof Interaction) : boolean {
	switch (interaction.commandName) {
		case 'help': return true
		case 'toggle': return true
		case 'hanime': return true
		case 'htags': return true
		default: return false
	}
}
export const commands: { [name: string]: typeof SlashCommand } =  {
	'help': new SlashCommandBuilder()
		.setName('help')
		.setDescription('Show help prompt'),
	'toggle': new SlashCommandBuilder()
		.setName('toggle')
		.setDescription('List available servers'),
	'ip': new SlashCommandBuilder()
		.setName('ip')
		.setDescription('Fetch the IP for the server'),
	'top': new SlashCommandBuilder()
		.setName('top')
		.setDescription('Gets the top thread for a specified board')
		.addStringOption(option =>
			option
				.setName('board')
				.setDescription('Board Id')
				.setRequired(true)),
	'pron': new SlashCommandBuilder()
		.setName('pron')
		.setDescription('Get DM\'d random porn thread or sfw thread'),
	'hanime': new SlashCommandBuilder()
		.setName('hanime')
		.setDescription('Regular text based Hanime search')
		.addStringOption(option =>
			option
				.setName('text')
				.setDescription('Text to search with')),
	'htags': new SlashCommandBuilder()
		.setName('htags')
		.setDescription('Lists out available tags for searching Hanime'),
}

export function help_command() : MessageEmbed {
	const msg = new MessageEmbed()
		.setTitle('**Available Commands**\n')
		.setDescription(':^)')
		.setColor(color.NORMAL)

	for(const name of Object.keys(commands)) {
		const cmd = commands[name]
		msg.addField(cmd.name, cmd.description)
	}

	return msg
}

export async function dispatch(interaction: CommandInteraction) : Promise<CommandResponse> {
	const name = interaction.commandName
	// Basically this is for the builtin commands
	switch(name) {
		case 'help': {
			return new CommandResponse(help_command(), Response.EphemeralReply)
		}
		case 'toggle': {
			// NOTE: Do not filter anything out for the dev server
			if(interaction.guild.id == config.dev_guild_id) {
				const content = await list_instances()
				return new CommandResponse(content, Response.EphemeralReply, true)
			} 
			// NOTE: Everyone else gets the filter
			const content = await list_instances(interaction.guild.id)
			return new CommandResponse(content, Response.EphemeralReply, true)
		}
		case 'top': {
			const arg = interaction.options.getString('board')
			return new CommandResponse(
				await top_thread(arg),
				Response.Dm)
		}
		case 'pron': {
			return new CommandResponse(await porn_roulette(), Response.Dm)
		}
		case 'hanime': {
			const raw = interaction.options.getString('text')
			const args = raw.split(' ')
			return await new CommandResponse(
				await text_search(args),
				Response.EphemeralReply)
		}
		case 'htags': {
			return new CommandResponse(htags(), Response.EphemeralReply)
		}
		default: 
			return new CommandResponse(help_command(),Response.EphemeralReply)
	}
}

