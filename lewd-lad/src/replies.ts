import { Response, CommandResponse, is_ephemeral, help_command } from './commands'
import { stdout } from './log'
const { Interaction } = require( '@discordjs/builders')

export enum DeferAction {
	Update,
	Reply,
	UpdateEphemeral,
	ReplyEphemeral,
	None
}

export async function smartdefer(interaction: typeof Interaction, action: DeferAction) {
	/**
	 * Figure out how to defer the message properly since updates & replies
	 * have to be deffered differently
	 */
	switch (action) {
		case DeferAction.Update : {
			await interaction.deferUpdate()
			break
		}
		case DeferAction.Reply : {
			await interaction.deferReply()
			break
		}
		case DeferAction.UpdateEphemeral : {
			await interaction.deferUpdate({ephemeral: true})
			break
		}
		case DeferAction.ReplyEphemeral : {
			await interaction.deferReply({ephemeral: true})
			break
		}
		default : { /* Nothing to do with no defer required */}
	}
}

export function get_action(interaction: typeof Interaction, select: boolean) : DeferAction {
	if(select) {
		if(is_ephemeral(interaction)) {
			return DeferAction.UpdateEphemeral
		}
		return  DeferAction.Update
	}
	if(is_ephemeral(interaction)) {
		return DeferAction.ReplyEphemeral
	}
	return DeferAction.Reply
}

export async function menu(interaction: typeof Interaction, reply: CommandResponse) {
	switch(reply.response_type) {
		case Response.Dm : {
			await interaction.user.send({
				content: 'dm',
				components: reply.content
			})
			await interaction.editReply({
				content: 'Check your DM\'s baby :kissing_heart:'
			})
			break
		}
		case Response.EphemeralReply : {
			await interaction.editReply({
				content: 'Make ur selection fam',
				components: [reply.content]
			})
			break
		}
		case Response.OpenReply : {
			await interaction.editReply({
				content: 'Open reply',
				components: [reply.content]
			})
			break
		}
	}
}



export async function embed(interaction: typeof Interaction, reply: CommandResponse) {
	switch(reply.response_type) {
		case Response.Dm : {
			await interaction.user.send({ embeds: [reply.content] })
			await interaction.editReply({
				content: 'Check your DM\'s baby :kissing_heart:'
			})
			break;
		}
		case Response.EphemeralReply : {
			await interaction.editReply({
				embeds: [reply.content]
			})
			break;
		}
		case Response.OpenReply : {
			await interaction.editReply({
				embeds: [reply.content]
			})
			break;
		}
	}
}

export async function default_help(interaction: typeof Interaction) {
	await interaction.editReply({ embeds: [help_command()]})
}
