import got from 'got'
import * as color from './colors'
import { stderr } from './log'
import { MessageEmbed } from 'discord.js'

class OpData {
	time: number|null
	subject: string|null
	comment: string|null

	constructor(post: unknown|null) {
		if(post == null) {
			this.time = null
			this.subject = null
			this.comment = null
		} else {
			this.time = post['time'] ? post['time'] : 'No time'
			this.subject = post['sub'] ? post['sub'] : 'No subject'
			this.comment = post['com'] ? post['com'] : 'No comment'
		}
	}
}

class ThreadMeta {
	board: string
	id: number
	last_modified: number
	replies: unknown
	constructor(board: string, id: number, last_modified: number, replies: unknown) {
		this.board = board
		this.id = id
		this.last_modified = last_modified
		this.replies = replies
	}

	apiLink() : string {
		return `https://a.4cdn.org/${this.board}/thread/${this.id}.json`
	}

	asLink() : string {
        return `https://boards.4chan.org/${this.board}/thread/${this.id}`
	}

	async getOpData() : Promise<OpData> {
		// Refer to https://github.com/4chan/4chan-API/blob/master/pages/Threads.md
		// for more info on what the thread endpoint actually returns
		const response = await got(this.apiLink()).json()
		return new OpData(response['posts'][0])
	}

}

async function threads_list(board_id:string) : Promise<Array<ThreadMeta>> {
	const url = `https://a.4cdn.org/${board_id}/threads.json`;
    const response: Array<{page: number, threads: Array<unknown>}> = await got(url).json();
	// Page 1 threads
	const threads = response[0].threads.map(
		(thread: {no: number, last_modified: number, replies: number}) => {
			return new ThreadMeta(
				board_id,
				thread.no,
				thread.last_modified,
				thread.replies
			)
		}
	)

    return threads
}

export async function top_thread(board: string) : Promise<MessageEmbed> {
    try {
		const threads = await threads_list(board)
		const meta_top = threads[1]
		// Top level thread meta contains barely anything so an extra fetch
		// is highly recommended/required to fill out the embed properly
		const link = meta_top.asLink()
		const op = await meta_top.getOpData()
		return new MessageEmbed()
			.setColor(color.NORMAL)
			.setTitle(link)
			.setDescription('>here\'s your content bruv')
			.addField('Subject', op.subject)
			.addField('Comment', op.comment.substr(0,100))
			.addField('Posted at', op.time.toString())
    }
    catch (err) {
		stderr('4chan-api-error', err)
		return new MessageEmbed()
			.setTitle('the mac minis are mad')
			.setDescription('4chans api prolly broke again')
    }
}

export async function porn_roulette() : Promise<MessageEmbed> {
	const boards = [ ['hm', 'u', 'y', 'd', 'e', 'aco', 'c'], ['wg', 'a', 'gd', '3', 'hr', 'an'] ];
	const _o = (Math.random() <= 0.80) ? 0 : 1;
	const board = boards[_o][Math.floor(Math.random() * boards[_o].length)];

	return threads_list(board).then(
		pages => {
			const thread = pages[Math.floor(Math.random() * pages.length)];
			return new MessageEmbed()
				.setTitle(thread.asLink())
				.setDescription('Here you go hot stuff')
		},
		err => {
			stderr('4chan-porn', err);
			return new MessageEmbed()
				.setTitle('mac minis are mad')
				.setDescription('welcome to bottom gear')
		}
	).catch(reason => {
		stderr('hanime-porn-roulette-err', reason)
		return new MessageEmbed()
			.setTitle('somehow its all gone to shit')
			.setDescription('welcome to the cum zone')
	});
}
