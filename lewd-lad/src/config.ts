import { stdout } from './log'

class ConfigError extends Error {
	constructor(message?: string) {
		super(message)
		this.name = 'ConfigError'
	}
}

export class CustomConfig {
	discord_id: string
	discord_token: string

	aws_api_id: string
	aws_api_region: string
	aws_api_stage: string
	aws_api_key: string

	debug_mode: boolean
	dev_guild_id: string
	beehive_id: string

	constructor(config_file_path?: string|null) {
		if (config_file_path) { 
			stdout('info', `Using ${config_file_path}`) 
		}

		if(config_file_path) {
			this._set_via_file(config_file_path)
		} else {
			this._set_via_env()
		}
	}
	private _set_via_file(path: string|null) {
		if(!path) {
			throw(new ConfigError(`Invalid path: ${path}`))
		}
		const keys = require(path)
		this.discord_id = keys['token']
		this.discord_token = keys['token']

		this.aws_api_id = keys['aws_api_id']
		this.aws_api_region = keys['aws_api_region']
		this.aws_api_stage = keys['aws_api_stage']
		this.aws_api_key = keys['aws_api_key']
		this.debug_mode = keys['debug'] != undefined
		this.dev_guild_id = keys['dev_guild_id']
		this.beehive_id = keys['beehive_id']
	}
	private _set_via_env() {
		this.discord_id = process.env['DISCORD_ID']
		this.discord_token = process.env['DISCORD_TOKEN']

		this.aws_api_id = process.env['AWS_API_ID']
		this.aws_api_region = process.env['AWS_API_REGION']
		this.aws_api_stage = process.env['AWS_API_STAGE']
		this.aws_api_key = process.env['AWS_API_KEY']
		this.debug_mode = process.env['DEBUG'] != undefined
		this.dev_guild_id = process.env['DEV_GUILD_ID']
		this.beehive_id = process.env['BEEHIVE_ID']
	}
}
