# Lewd Lad

The best pronz chat bot turned server manager you've ever seen.

This bot is used heavily in the Left Coast Community as a way 
to manage the many game servers that I host on AWS.

In order to save time bringing servers up & down all day this
chat bot is given special access to an AWS VPC which it then
checks for running servers.

## Word of warning

At this time I have no intentions of making this whole system
package-able so if that is something you really want send me some
koin on my [Ko-fi]() ;) jk

Do send me a message here `` and we talk about how to handle packaging
because this project is effectively massive at this point and requires
special care to setup in full.

## Tech Stack

* NodeJS/Typescript

The Bot itself it is written in Typescript with some decently strict
compiler options(within reason at least).

* AWS 

Infrastructure is deployed via Terraform however because I need this infra
to literally never move my CI doesn't ever touch this on its own.

* Terraform

If you want the same server network/infrastructure you're going basically
need the same kind of setup that I have. In reality a better setup can be made
with some load balancing and tricks w/ reverse proxies for more flexibility
but this infrastructure should work for those that don't want to setup
all of that.

* AWS Lambda

This is used to deploy an API which Lewdlad uses to actually control the
EC2 instances. To make a good API key for the API I recommend using: 

```
size=128
cat /dev/urandom | tr -dc [:alnum:] | fold -w $size | head -1
```

Large, crypographically random, and super easy to change out. Thank me later.
If you need to increase the size of th key then just change the `size`
variable.

* Ansible

Used for docker image deployment. I should note the scripts in this directory
are just mirrored from their actual home in [Project Athens](https://gitlab.com/shockrah/project-athens).

Suggestion: Check that repository for more Ansible goodness, like setting up
a docker host ;)

* Docker

Because I value my sanity a `Dockerfile` is used to create a Docker image
which I host in this project's container registry.

The following flags can be passed as environment variables(**all as strings**):

* DISCORD_ID

* DISCORD_TOKEN

* AWS_API_ID

* AWS_API_STAGE

* AWS_API_REGION

* AWS_API_KEY

The API keys are for the lambda API so that the bot can talk to lambda
successfully, lest it 401 forever.

