# All the input variables that are used throughout with descriptions
# values are found in secrets.tfvars

# Sensitive flag is used to hide the values in build logs

######################## VPC CONFIGURATIONS     ######################## 

variable "bucket_cidr" {
  description = "VPC Cidr block"
  type = string
  default = "10.0.0.0/16"
}
variable "milk_cidr" {
  description = "Public Subnet Cidr block"
  type = string
  default = "10.0.1.0/24"
}

######################## GENERAL CONFIGURATIONS ######################## 
variable "blocks_ami" {
  description = "Main AMI used for game servers"
  type = string
  sensitive = true
}
variable "availability_zone" {
  description = "VPC Availability zone"
  type = string
  sensitive = true
}

variable "loki_ingest_port" {
  description = "Port used to send data to Loki(Bee Hive)"
  type = number
  default = 3100
}

################ BEE HIVE CONFIGURATIONS ################ 

variable "bee_hive_private_ip" {
	description = "Internal IP for beehive (logging server)"
	type = string
	default = "10.0.1.10"
}

variable "bee_hive_machine_type" {
  description = "Special requirements require a special machine type"
  type = string
  sensitive = true
  default = "t3.micro"
}
variable "bh_ssh_key_name" {
  description = "Bee hive's SSH key name"
  type = string
  sensitive = true
}
variable "bh_ssh_key_path" {
  description = "Path to public key"
  type = string
  sensitive = true
}


################ ENDSTONE CONFIGURATIONS ################ 

variable "endstone_private_ip" {
	description = "Internal IP for endstone(reflex)"
	default = "10.0.1.11"
	type = string
}

variable "endstone_machine_type" {
  description = "Reflex server machine type(likely smaller than most others)"
  type = string
  sensitive = true
}
variable "endstone_ssh_key_name" {
  description = "Endstone's SSH key name"
  type = string
  sensitive = true
}
variable "endstone_ssh_key_path" {
  description = "Path to public key"
  type = string
  sensitive = true
}
variable endstone_game_port {
  description = "Endstone's main game port(Reflex)"
  type = number
  default = 25787
}

variable "endstone_dns" {
  description = "Endstone's DNS A record name"
  type = string
}

################ TROPICAL FISH CONFIGURATIONS ################ 

variable "tropical_fish_private_ip" {
	description = "Internal IP for tropical fish(minecraft 1.18.1)"
	default = "10.0.1.12"
	type = string
}
variable "tropical_fish_machine_type" {
  # NOTE: this is the base level which is the smaller size the server
  # is allowed to run on. The elastic nature of reqs here means this type
  # may change during the box's lifetime
  description = "TF's machine type(Likely larger than normal)"
  type = string
  sensitive = true
}
variable "tf_ssh_key_name" {
  description = "TF's SSH key name"
  type = string
  sensitive = true
}

variable "tf_ssh_key_path" {
  description = "Path to public key"
  type = string
  sensitive = true
}

variable tropical_fish_mc_port {
  description = "Tropical Fish's minecraft ports"
  type = number
  default = 25565
}

variable "tropicalfish_dns" {
  description = "Tropicalfish's DNS A record name"
  type = string
}

######################## AWS PROVIDER CONFIG ######################## 

variable "aws_key" {
  description = "API Key for AWS"
  type = string
  sensitive = true
}
variable "aws_secret" {
  description = "AWS secret key"
  type = string
  sensitive = true
}
variable "milk_bucket_region" {
  description = "Milk Bucket VPC Region"
  type = string
  sensitive = true
}

######################## ARCHITECTURE VARS ######################## 
variable "left_coast_guild_id" {
	description = "Left Coast Guild ID"
	type = string
	sensitive = true
}

variable "shark_group_guild_id" {
  description = "Shark group discord guild id"
  type = string
  sensitive = true
}

