# Old infra on AWS

Basically tropical fish is the only server we care about backing up before
ripping it down. To fully clean this up we're going to back up the minecraft
world data to a private on-prem server which is not public for now since we're
going to be moving off to Vultr very soon.
