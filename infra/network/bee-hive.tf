# This ruleset defines the rules for the main logging server which runs Loki
# for its primary data ingestion system. It only accepts network based logging
# traffic as we're meant to tunnel into this server in order to use Grafana.
resource "aws_security_group" "bee_hive_sec" {
  vpc_id = aws_vpc.milk_bucket.id

  # Only accepting connections on 3100 for Loki
  # Grafana(3000) will be accessed via local tunnel to avoid memery w/ certs
  ingress {
    cidr_blocks = ["10.0.1.0/24"]
    from_port = var.loki_ingest_port
    to_port = var.loki_ingest_port
    protocol = "tcp"
  }
  # Internet SSH for bee hive
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 22
    to_port = 22
    protocol = "tcp"
  }

  tags = {
    Name = "Bee Hive Security Group"
    Description = "Ingress is allowed for SSH as the Grafana console should be accessed over SSH tunnels."
  }
}
# This component basically takes care of the logging server
# which primarily hosts dockerized Grafana and Loki
# This will have to have it's own dynamically allocated

resource "aws_key_pair" "bee_hive_ssh" {
  key_name = var.bh_ssh_key_name
  public_key = file(var.bh_ssh_key_path)
}

resource "aws_instance" "bee_hive" {
  ami = var.blocks_ami
  instance_type = var.bee_hive_machine_type

  subnet_id = aws_subnet.milk_bucket_subnet.id
  private_ip = var.bee_hive_private_ip
  associate_public_ip_address = true

  key_name = var.bh_ssh_key_name

  vpc_security_group_ids = [
    aws_security_group.bee_hive_sec.id,
    aws_security_group.common_logging.id,
    aws_security_group.common_web_access.id
  ]

  lifecycle {
    ignore_changes = [
      associate_public_ip_address,
      instance_state,
      public_dns,
      public_ip
    ]
  }

  tags = {
    Name = "Bee Hive"
  }
}
