# This module deals with all the relevant internet routing required to
# get the inner hosts the interacess they require to operate

resource "aws_internet_gateway" "nether_portal_igw" {
  vpc_id = aws_vpc.milk_bucket.id
  tags = {
    Name = "IGW - Nether Portal"
  }
}

resource "aws_route_table" "nether_portal" {
  vpc_id = aws_vpc.milk_bucket.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.nether_portal_igw.id
  }
  tags = {
    Name = "Nether Portal Route Table"
  }
  depends_on = [ aws_internet_gateway.nether_portal_igw ]
}

resource "aws_route_table_association" "nether_portal" {
  subnet_id = aws_subnet.milk_bucket_subnet.id
  route_table_id = aws_route_table.nether_portal.id
}