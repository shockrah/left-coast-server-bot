######################## COMMON SECURITY GROUPS ############

# Ruleset allowing servers to send logs to Loki service running on the same
# network.
resource "aws_security_group" "common_logging" {
  vpc_id = aws_vpc.milk_bucket.id
  # Egress to local network
  egress {
    cidr_blocks = ["10.0.1.0/24"]
    from_port = 3100
    to_port = 3100
    protocol = "tcp"
  }

  tags = {
    Name = "Bee Hive Logging Egress"
    Description = "Allows intranet servers to log things to Loki"
  }
}

# Common ruleset that servers need to be able to update themselves via apt
resource "aws_security_group" "common_web_access" {
  vpc_id = aws_vpc.milk_bucket.id
  # Egress for APT
  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 443
    to_port = 443
    protocol = "tcp"
  }

  # WARN: only because APT repo's sometimes fallback to port 80
  # Most traffic won't(shouldn't) be on this port
  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 80
    to_port = 80
    protocol = "tcp"
  }

  tags = {
    Name = "Common Web Egress"
    Description = "Used to allow servers access to the internet for updates"
  }
}

