# This new vpc is where we are going to keep all future instances for this
# project. The main distinction is that this time we'll have a clean slate
# to work with, rather than something which is "tainted"
#
resource "aws_vpc" "milk_bucket" {
	cidr_block = var.bucket_cidr
	enable_dns_hostnames = true
	enable_dns_support = true

  tags = {
    Name = "Milk Bucket"
  }
}

# The main reason we're going with a single subnet for this vpc is
# strictly for cost reasons. while a public/private setup might be better
# from a security standpoint the cost is way to high to justify.
resource "aws_subnet" "milk_bucket_subnet" {
  vpc_id = aws_vpc.milk_bucket.id
  cidr_block = var.milk_cidr
  availability_zone = var.availability_zone

  tags = {
    Name = "Milk Bucket Subnet"
  }
}
