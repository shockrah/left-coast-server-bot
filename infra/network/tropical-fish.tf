# Minecraft server - the one with the beach houses

# Ruleset for Tropical Fish
# This covers minecraft 1.18 default ports. Other minecraft servers will use
# alternate ports to avoid collision on the same EIP.
resource "aws_security_group" "tropical_fish_sec" {
  vpc_id = aws_vpc.milk_bucket.id

  # MC requires two rules on the same port for both TCP & UDP
  ingress { 
    cidr_blocks = ["0.0.0.0/0"]
    from_port = var.tropical_fish_mc_port
    to_port = var.tropical_fish_mc_port
    protocol = "tcp"
  }
  ingress { 
    cidr_blocks = ["0.0.0.0/0"]
    from_port = var.tropical_fish_mc_port
    to_port = var.tropical_fish_mc_port
    protocol = "udp"
  }
  # Logging to Loki
  egress {
    cidr_blocks = ["10.0.1.0/24"]
    from_port = var.loki_ingest_port
    to_port = var.loki_ingest_port
    protocol = "tcp"
  }

  # SSH from the internet
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 22
    to_port = 22
    protocol = "tcp"
  }

  tags = {
    Name = "Tropical Fish SecGroup"
  }
}


resource "aws_key_pair" "tropical_fish_ssh" {
  key_name = var.tf_ssh_key_name
  public_key = file(var.tf_ssh_key_path)
}

resource "aws_instance" "tropical_fish" {
  ami = var.blocks_ami
  instance_type = var.tropical_fish_machine_type

  subnet_id = aws_subnet.milk_bucket_subnet.id
  private_ip = var.tropical_fish_private_ip
  associate_public_ip_address = true

  key_name = var.tf_ssh_key_name

  vpc_security_group_ids = [
    aws_security_group.tropical_fish_sec.id,
    aws_security_group.common_logging.id,
    aws_security_group.common_web_access.id
  ]

  lifecycle {
    ignore_changes = [
      associate_public_ip_address,
      instance_state,
      public_dns,
      public_ip
    ]
  }

  tags = {
    Name = "Tropical Fish"
    Description = "Minecraft 1.18.1"
    "${var.left_coast_guild_id}" = "Left Coast Guild ID"
    connection_string = var.tropicalfish_dns
  }
}



