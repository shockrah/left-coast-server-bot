resource "aws_iam_role" "ecs_task_role" {
  name = "${var.ecs_name}-ecsTaskRole"
 
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
        {
            Action = "sts:AssumeRole"
            Principal = {
                Service = [
                  "lambda.amazonaws.com",
                  "ecs-tasks.amazonaws.com"
                ]
            }
            Effect = "Allow"
        }
    ]
  })
}

# Our own policy for managing allowed actions from lambda - ecs
resource "aws_iam_policy" "lambda_exec_ecs" {
  name = "${var.ecs_name}-ecsLambdaExec"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "iam:PassRole",
          "ecs:RunTask",
          "ecs:StopTask",
          "ecs:ListAttributes",
          "ecs:StartTask",
          "ecs:DescribeTaskDefinition",
          "ecs:DescribeClusters",
          "ecs:ListServices",
          "ecs:ListTagsForResource",
          "ecs:ListTasks",
          "ecs:ListTaskDefinitionFamilies",
          "ecs:StopTask",
          "ecs:DescribeServices",
          "ecs:ListContainerInstances",
          "ecs:DescribeContainerInstances",
          "ecs:DescribeTasks",
          "ecs:ListTaskDefinitions",
          "ecs:DescribeTaskDefinitions",
          "ecs:ListClusters",
          # Logging
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:DescribeLogStreams",
          "logs:PutLogEvents",
        ]
        Resource = "*"
        Effect = "Allow"
      }
    ]
  })
}

resource "aws_iam_policy" "witch_hut_efs_clients" {
  name = "${var.ecs_name}-ecs-write-disk"
  policy = jsonencode({
    "Version"    = "2012-10-17"
    "Statement"  = [
      {
        "Effect" = "Allow"
        "Action" = [
          "elasticfilesystem:ClientMount",
          "elasticfilesystem:ClientWrite",
          "elasticfilesystem:DescribeFileSystems"
        ]
        "Resource" = "*"
      }
    ]
  })
}

# Attach our own policies to our witch hut role
resource "aws_iam_role_policy_attachment" "ecs-task-execution-role-policy-attachment" {
  role       = aws_iam_role.ecs_task_role.name
  policy_arn = aws_iam_policy.lambda_exec_ecs.arn
}

resource "aws_iam_role_policy_attachment" "witch-hut-efs-role-policy-attachment" {
  role       = aws_iam_role.ecs_task_role.name
  policy_arn = aws_iam_policy.witch_hut_efs_clients.arn
}


# Attach a managed ECS policy to our role
resource "aws_iam_role_policy_attachment" "ecr-auth-policy" {
  role       = aws_iam_role.ecs_task_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}


