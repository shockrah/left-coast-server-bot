# Here we're just pulling values from ssm that are required to run

data "aws_ssm_parameter" "discord_id"     { name = "lewdlad-discord-id" }
data "aws_ssm_parameter" "discord_token"  { name = "lewdlad-discord-token" }
data "aws_ssm_parameter" "api_id"     { name = "lewdlad-api-id" }
data "aws_ssm_parameter" "api_stage"  { name = "lewdlad-api-stage" }
data "aws_ssm_parameter" "api_region" { name = "lewdlad-api-region" }
data "aws_ssm_parameter" "api_key"    { name = "lewdlad-api-key" }
data "aws_ssm_parameter" "dev_guild_id"   { name = "lewdlad-dev-guild-id" }
data "aws_ssm_parameter" "beehive_id"     { name = "lewdlad-beehive-id" }
