resource "aws_ecs_service" "lewdlad" {
  name            = "${var.ecs_name}-lewdlad-service"
  cluster         = aws_ecs_cluster.witch_hut.id
  task_definition = aws_ecs_task_definition.lewdlad.arn

  # No autoscaling as it's not needed
  desired_count   = 1
  launch_type     = "FARGATE"

  network_configuration {
    assign_public_ip = true
    subnets = [ var.milk_bucket_subnet ]
    security_groups = [ aws_security_group.base_ecs.id ]
  }
}

