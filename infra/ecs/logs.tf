resource "aws_cloudwatch_log_group" "wh_logs" {
  name = var.witch_hut_log_group
  # 2 weeks of retention is more than enough for our purposes
  retention_in_days = "14"
  tags = {
    Name = var.witch_hut_log_group
    Description = "Logging group for all witch hut tasks"
  }
}

