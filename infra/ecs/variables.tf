######################## AWS PROVIDER CONFIG ######################## 

variable "aws_key" {
  description = "API Key for AWS"
  type = string
  sensitive = true
}
variable "aws_secret" {
  description = "AWS secret key"
  type = string
  sensitive = true
}

######################## DEPENDENT VARIABLES ######################## 

variable "milk_bucket_region" {
  description = "Milk Bucket VPC Region"
  type = string
  sensitive = true
}

######################## CLUSTER VARIABLES ######################## 
variable "ecs_name" {
  description = "Name of ECS cluster"
  type = string
  sensitive = false
  default = "witch_hut"
}

variable "tropical_fish_image" {
  description = "Image to use for TF containers"
  type = string
  sensitive = true
}

variable "milk_bucket_subnet" {
  description = "Public subnet ID"
  type = string
  sensitive = true
}

variable "milk_bucket_vpc" {
  description = "Milk Bucket VPC ID"
  type = string
  sensitive = true
}

variable "witch_hut_log_group" {
  description = "Name of witch hut logging group"
  type = string
  default = "WitchHut-Logs"
}

######################## TROPICAL FISH VARIABLES ######################## 
variable "left_coast_guild_id" {
  description = "Left coast guild ID in Discord"
  type = string
  sensitive = true
}
