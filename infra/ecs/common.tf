# TODO This doesn't have a public IP so it (((should))) be ok to keep this super
# open although if this can be tightened then it really should
resource "aws_security_group" "common_efs" {
  vpc_id = var.milk_bucket_vpc
  ingress {
    cidr_blocks = [ "0.0.0.0/0" ]
    from_port   = 2049
    to_port     = 2049
    protocol    = "tcp"
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [ "0.0.0.0/0" ]
  }

  name = "Witch-Hut Common EFS"
  description = "EFS SG allowing NFS inbound"
}


# This SG is the base SG for all tasks to use
# Additional SG rules are setup on a case by case basis
resource "aws_security_group" "base_ecs" {
  vpc_id = var.milk_bucket_vpc
  egress {
    cidr_blocks = [ "0.0.0.0/0" ]
    from_port = 443
    to_port = 443
    protocol = "tcp"
  }
  egress {
    cidr_blocks = [ "0.0.0.0/0" ]
    from_port   = 2049
    to_port     = 2049
    protocol    = "tcp"
  }

  name = "Witch Hut Base"
  description = "For tasks to run correctly"
}
