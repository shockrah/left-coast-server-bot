locals {
  cpu = 256
  mem = 512
}

# Here we are defining how the service for lewdlad is to be built
# Because this is nothing more than a single container it means
# We can get away with having a very simple service
resource "aws_ecs_task_definition" "lewdlad" {
  family = "${var.ecs_name}-lewdlad-task"

  network_mode              = "awsvpc"
  requires_compatibilities  = [ "FARGATE" ]
  execution_role_arn        = aws_iam_role.ecs_task_role.arn

  cpu     = local.cpu
  memory  = local.mem

  container_definitions = jsonencode([
    {
      name      = "lewdlad"
      image     = "registry.gitlab.com/shockrah/left-coast-server-bot:latest"
      cpu       = local.cpu
      memory    = local.mem
      essential = true
      environment = [
        { name = "DISCORD_ID",      value = data.aws_ssm_parameter.discord_id.value },
        { name = "DISCORD_TOKEN",   value = data.aws_ssm_parameter.discord_token.value },
        { name = "AWS_API_ID",      value = data.aws_ssm_parameter.api_id.value },
        { name = "AWS_API_STAGE",   value = data.aws_ssm_parameter.api_stage.value },
        { name = "AWS_API_REGION",  value = data.aws_ssm_parameter.api_region.value },
        { name = "AWS_API_KEY",     value = data.aws_ssm_parameter.api_key.value },
        { name = "DEV_GUILD_ID",    value = data.aws_ssm_parameter.dev_guild_id.value },
        { name = "BEEHIVE_ID",      value = data.aws_ssm_parameter.beehive_id.value },
      ]
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group         = aws_cloudwatch_log_group.wh_logs.name
          awslogs-region        = var.milk_bucket_region
          awslogs-stream-prefix = "${var.ecs_name}-lewdlad-container"
        }
      }
    }
  ])
}

