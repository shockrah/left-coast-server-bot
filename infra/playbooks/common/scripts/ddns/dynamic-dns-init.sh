#!/bin/sh

# This script will basically set the A record for the server when it starts up
echo "Sleeping for 10 seconds to allow network to fully rise"
sleep 10
set -e

if [ -z $LEFT_COAST_ZONE_ID ]; then
	echo Dynamic DNS could not be setup as LEFT_COAST_ZONE_ID was not set
	exit 1
fi

# First we need the instance and public ip of the host
public_ip="`curl ifconfig.me`"
tmp_dns_json=/tmp/init-dns.json


# Create a resource with our desired name
cat << EOF > $tmp_dns_json
{
	"Comment": "UPSERT A record for $DOMAIN",
	"Changes": [
		{
			"Action": "UPSERT",
			"ResourceRecordSet": {
				"Name": "$DOMAIN",
				"Type": "A",
				"TTL": 300,
				"ResourceRecords": [
					{
						"Value": "$public_ip"
					}
				]
			}
		}
	]
}
EOF

aws route53 change-resource-record-sets \
	--hosted-zone-id $LEFT_COAST_ZONE_ID \
	--change-batch file://$tmp_dns_json

rm -f $tmp_dns_json

