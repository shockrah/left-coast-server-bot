#!/bin/sh

# This script is used to remove A records in the targeted hosted zone
# when a server is shut down. We do this because when the IP's are cleaned
# up we no longer own them so we want to make sure that we don't accidently
# leave a hanging pointer to some poor random sod that gets our IP


tmp_dns_json=/tmp/end-dns.json
public_ip="`curl ifconfig.me`"

cat << EOF > $tmp_dns_json
{
	"Comment": "Delete A record for $DOMAIN",
	"Changes": [
		{
			"Action": "DELETE",
			"ResourceRecordSet": {
				"Name": "$DOMAIN",
				"Type": "A",
				"TTL": 300,
				"ResourceRecords": [
					{
						"Value": "$public_ip"
					}
				]
			}
		}
	]
}
EOF

aws route53 change-resource-record-sets \
	--hosted-zone-id $LEFT_COAST_ZONE_ID \
	--change-batch file://$tmp_dns_json

rm -f $tmp_dns_json
