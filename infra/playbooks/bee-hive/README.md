Role Name
=========

This role contains everything required to setup the bee-hive instance.
The only reason why this server is logically separated from the game
servers is because the requirements are so wildly different from
an operational perspective.


Role Variables
--------------

All variables that can be set for this role are set in `vars/main.yml`.
Configuration of services like Grafana/Loki will be done via the web
interface as they are one time setups and can not be reasonably configured
through `vars/main.yml`.

Dependencies
------------

This role also uses the docker plugin to setup Grafana and Loki on the
server.

License
-------

GPL-3.0

Author Information
------------------

Shockrah

* email: `dev@shockrah.xyz`


