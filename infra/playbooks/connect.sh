
beehive() {
	set -x
	ssh -i ../keys/bee-hive/bee-hive -L 3000:localhost:3000 \
		-o UserKnownHostsFile=/dev/null \
		ubuntu@beehive.leftcoast.space
}

tropicalfish() {
	set -x
	ssh -i ../keys/tropical-fish/tropical-fish \
		-o UserKnownHostsFile=/dev/null \
		ubuntu@tropicalfish.leftcoast.space
}

endstone() {
	set -x
	ssh -i ../keys/endstone/endstone \
		-o UserKnownHostsFile=/dev/null \
		ubuntu@endstone.leftcoast.space
}

if [ -z "$1" ]; then
cat << EOF
Usage:
	connect.sh beehive
		SSH + forwards Grafana port for dashboard acess
	connect.sh tropicalfish
		Simple shell acess
	connect.sh endstone
		Simple shell acess
EOF
else 
	"$@"
fi
