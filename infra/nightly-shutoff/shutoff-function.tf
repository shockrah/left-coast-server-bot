# This modue is basically all we need to shutoff everything every night at
# roughly 1AM PST


resource "aws_lambda_function" "shutoff" {
  function_name = "nightly-shutoff"
  role = var.manage_instances_role

  runtime = "python3.8"
  filename = "src/package.zip"
  handler = "main.lambda_handler"

  environment {
    variables = {
      VPC_ID = var.target_vpc
    }
  }
  tags = {
    Description = "Shutoff all servers at 1AM PST every day"
  }
}


