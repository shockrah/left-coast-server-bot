#!/bin/bash
# First thing to do is make sure we have the required dependencies
set -ex
pushd src/
	if [ ! -d package ]; then
		pip install --target ./package boto3
	fi
	pushd package/
		zip -r ../package.zip .
	popd
	zip -g package.zip main.py
popd
