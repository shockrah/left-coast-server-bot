resource "aws_cloudwatch_event_rule" "routine_caller" {
  name = "daily-1am-shutoff"
  description = "Calls the shutoff lambda function every day at 1AM"

  schedule_expression = "cron(45 9 * * ? *)"
}

resource "aws_cloudwatch_event_target" "trigger_lambda" {
  rule = aws_cloudwatch_event_rule.routine_caller.name
  arn  = aws_lambda_function.shutoff.arn
}
