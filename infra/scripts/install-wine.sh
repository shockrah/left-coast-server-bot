#!/bin/sh

# This script aims to setup wine in the most painless way possible since 
# reflex servers are dumb and require wine for some reason


repo_setup() {
	dpkg --add-architecture i386
	wget -nc https://dl.winehq.org/wine-builds/winehq.key
	apt-key add winehq.key

	echo 'deb https://dl.winehq.org/wine-builds/debian/ buster main' \
		>> /etc/apt/sources.list.d/winehq.list
}

apt_setup() {
	apt update
	apt install --install-recommends winehq-stable
	apt install gnupg
}

if [ -z "$1" ];then
	echo No target game directory given!
	exit 1
fi

if [ ! root = `whoami`];then
	echo Please run this as root so we can install things on the system
	exit 1
fi


target="$1"
mkdir -p "$target"
cd "$target"
	repo_setup
	apt_setup
cd ../
