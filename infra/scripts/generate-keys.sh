#!/bin/sh

set -x
# This script basically just generates RSA keys for all
# members of the Milk Bucket VPC

members="bee-hive tropical-fish"

for member in $members;do 
	mkdir -p keys/$member
	timestamp=`date +'%D-%R'`
	ssh-keygen \
		-t rsa \
		-C "$member $timestamp" \
		-N "" \
		-f keys/$member/$member
done

