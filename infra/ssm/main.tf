# Here we define a bunch of params that we want to store for containers
# and other things that require secure key access

# Here we build out all the values that need to be set as sensitive
locals {
  prefix = "lewdlad"
}

resource "aws_ssm_parameter" "api-id" {
  name  = "${local.prefix}-api-id"
  value = var.api_id
  type  = "SecureString"
}
resource "aws_ssm_parameter" "api-key" {
  name  = "${local.prefix}-api-key"
  value = var.api_key
  type  = "SecureString"
}
resource "aws_ssm_parameter" "api-stage" {
  name  = "${local.prefix}-api-stage"
  value = var.api_stage
  type  = "SecureString"
}
resource "aws_ssm_parameter" "api-region" {
  name  = "${local.prefix}-api-region"
  value = var.api_region
  type  = "SecureString"
}
resource "aws_ssm_parameter" "beehive-id" {
  name  = "${local.prefix}-beehive-id"
  value = var.beehive_id
  type  = "SecureString"
}
resource "aws_ssm_parameter" "dev-guild-id" {
  name  = "${local.prefix}-dev-guild-id"
  value = var.dev_guild_id
  type  = "SecureString"
}
resource "aws_ssm_parameter" "discord-id" {
  name  = "${local.prefix}-discord-id"
  value = var.discord_id
  type  = "SecureString"
}
resource "aws_ssm_parameter" "discord-token" {
  name  = "${local.prefix}-discord-token"
  value = var.discord_token
  type  = "SecureString"
}
