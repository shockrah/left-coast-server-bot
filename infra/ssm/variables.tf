######################## AWS PROVIDER CONFIG ######################## 

variable "aws_key" {
  description = "API Key for AWS"
  type = string
  sensitive = true
}
variable "aws_secret" {
  description = "AWS secret key"
  type = string
  sensitive = true
}

######################## DEPENDENT VARIABLES ######################## 

variable "milk_bucket_region" {
  description = "Milk Bucket VPC Region"
  type = string
  sensitive = true
}

######################## LEWDLAD VARIABLES ######################### 
variable "api_id" {
  type = string
  sensitive = true
}

variable "api_key" {
  type = string
  sensitive = true
}

variable "api_stage" {
  type = string
  sensitive = true
}

variable "api_region" {
  type = string
  sensitive = true
}

variable "beehive_id" {
  type = string
  sensitive = true
}

variable "dev_guild_id" {
  type = string
  sensitive = true
}

variable "discord_id" {
  type = string
  sensitive = true
}

variable "discord_token" {
  type = string
  sensitive = true
}

