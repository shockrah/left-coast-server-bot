######################## AWS PROVIDER CONFIG ######################## 

variable "aws_key" {
  description = "API Key for AWS"
  type = string
  sensitive = true
}
variable "aws_secret" {
  description = "AWS secret key"
  type = string
  sensitive = true
}
variable "milk_bucket_region" {
  description = "Milk Bucket VPC Region"
  type = string
  sensitive = true
}
