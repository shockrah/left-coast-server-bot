resource "aws_route53_zone" "leftcoast_space" {
  name = "leftcoast.space"
  comment = "Zone for leftcoast.space domain"
}
