# Permissions for all integrations to do things with lambda
###########################################################

resource "aws_lambda_permission" "allow_integration" {
  for_each = toset([
    aws_lambda_function.start.function_name,
    aws_lambda_function.stop.function_name,
    aws_lambda_function.list.function_name,
  ])

  statement_id  = "AllowAPIgatewayInvokation"
  action        = "lambda:InvokeFunction"
  function_name = each.key
  principal     = "apigateway.amazonaws.com"

  source_arn = "arn:aws:execute-api:${var.milk_bucket_region}:${var.aws_account_id}:${aws_api_gateway_rest_api.lewdapi.id}/*"
}

