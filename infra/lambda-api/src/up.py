import state
import os
import boto3


REGION      = 'us-west-1'
BEEHIVE_ID  = os.environ.get('BEEHIVE_ID')

def error(msg: str):
    return {
        'Reservations': [],
        'ApiInputError': msg
    }

def handler(event: dict, context: dict):
    instance_id = event.get('instance-id')
    if instance_id:
        # Make sure we don't target bee hive ever
        if instance_id == BEEHIVE_ID:
            return error('No instance-id found in query string')
        # Next we can start up the target instance _and_ beehive assuming all
        # goes well
        client = boto3.client('ec2')
        # We do this twice because we don't want to start bee hive all by itself
        client.start_instances(InstanceIds=[instance_id])
        client.start_instances(InstanceIds=[BEEHIVE_ID])
    else:
        error('No instance-id found in query string')

