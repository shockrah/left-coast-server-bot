import os
import boto3
import json

REGION      = 'us-west-1'
VPC         = os.environ.get('TARGET_VPC')
def handler(event: dict, context: dict):
    client = boto3.client('ec2')
    pager = client.get_paginator('describe_instances')
    response = pager.paginate(
        Filters=[
            {
                'Name': 'vpc-id',
                'Values': [VPC]
            }
        ]
    )
    response = response.build_full_result()
    # We do this because there are datetime objects which do not serialize very
    # well and amazon can't figure out how to serialize them at all
    payload_s = json.dumps(response, default = str)
    payload = json.loads(payload_s)
    return payload



