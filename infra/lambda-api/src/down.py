import state
import os
import boto3

# Some constants
REGION      = 'us-west-1'
VPC         = os.environ.get('VPC_ID')
BEEHIVE_ID  = os.environ.get('BEEHIVE_ID')

def live_instances():
    '''
    Here we get a list of all live instances in our target vpc
    We do this because we need to know if there is more than 1 game server
    up so that we don't accidently take down bee hive without reason
    '''
    ec2 = boto3.resource('ec2', region_name=REGION)
    instances = [instance for instance in ec2.instances.all()]
    live = []
    for i in instances:
        in_vpc = i.vpc_id == VPC
        running = i.state['Code'] == state.running
        if in_vpc and running:
            live.append(i.id)

    return live

def handler(event: dict, context: dict):
    '''
    Main handler for smartly turning off servers 
    '''
    target = event['instance-id']
    # First we shut off the target instance(unless its bee hive
    if target == BEEHIVE_ID:
        print('[ WARN ] BEE HIVE Id given not shutting anything down')
    # Only try shutting things down when we're given a good instance ID
    else:
        client = boto3.client('ec2')
        client.stop_instances(InstanceIds=[target])
        # Finally make sure bee hive is the last one standing before
        # we take it down. As it should only live for other servers
        live = live_instances()
        if len(live) == 1:
            if live[0] == BEEHIVE_ID:
                client = boto3.client('ec2')
                client.stop_instances(InstanceIds=live)

