resource "aws_api_gateway_request_validator" "request_validation" {
  name = "generic-request-validation"
  rest_api_id = aws_api_gateway_rest_api.lewdapi.id
  validate_request_body       = true
  validate_request_parameters = true
}

