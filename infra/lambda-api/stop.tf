# This module covers the resources required for the POST /stop endpoint

resource "aws_api_gateway_resource" "stop_res" {
  rest_api_id = aws_api_gateway_rest_api.lewdapi.id
  parent_id   = aws_api_gateway_rest_api.lewdapi.root_resource_id
  path_part   = var.stop_path
}

resource "aws_api_gateway_method" "post_stop" {
  authorization = "NONE"
  http_method   = "POST"
  resource_id   = aws_api_gateway_resource.stop_res.id
  rest_api_id   = aws_api_gateway_rest_api.lewdapi.id
  api_key_required = true
  request_validator_id = aws_api_gateway_request_validator.request_validation.id
  request_parameters = {
    "method.request.querystring.instance-id" = true
  }
}


# Lambda
########

resource "aws_lambda_function" "stop" {
    function_name = "stop-instance"
    handler = "down.handler"
    runtime = "python3.8"
    filename = "src/package.zip"
    role = var.manage_instances_role
    # Need moar time since boto3 isn't fucking async anymore >:(
    timeout = 9
    environment {
      variables = {
        BEEHIVE_ID = var.beehive_id
        VPC_ID = var.target_vpc
      }
    }
    tags = {
      Description = "Stop EC2 instance in the target VPC"
    }
}

# Integrations
##############

resource "aws_api_gateway_integration" "stop_integration" {
  rest_api_id = aws_api_gateway_rest_api.lewdapi.id
  resource_id = aws_api_gateway_method.post_stop.resource_id

  http_method = aws_api_gateway_method.post_stop.http_method
  integration_http_method = "POST"

  type = "AWS"
  uri  = aws_lambda_function.stop.invoke_arn
  request_parameters = {
    "integration.request.querystring.instance-id" = "method.request.querystring.instance-id"
  }
  request_templates = {
    "application/json" = <<-EOF
      {
        "instance-id": "$input.params('instance-id')"
      }
    EOF
  }
}

# Response Configuration
########################

resource "aws_api_gateway_method_response" "stop_response" {
  rest_api_id = aws_api_gateway_rest_api.lewdapi.id
  resource_id = aws_api_gateway_resource.stop_res.id
  http_method = aws_api_gateway_method.post_stop.http_method
  status_code = "200"
  response_models = {
    "application/json" = "Empty"
  }
}

resource "aws_api_gateway_integration_response" "stop" {
  rest_api_id = aws_api_gateway_rest_api.lewdapi.id
  resource_id = aws_api_gateway_resource.stop_res.id
  http_method = aws_api_gateway_method.post_stop.http_method
  status_code = aws_api_gateway_method_response.stop_response.status_code
  depends_on = [
    aws_api_gateway_integration.stop_integration
  ]
}




