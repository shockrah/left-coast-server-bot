# here we define the api gateway
# that lewdlad will use, lambda functions
# are the backend part of this


resource "aws_api_gateway_rest_api" "lewdapi" {
    name = "lewd-api"
    description = "REST API for Lewdlad"
    endpoint_configuration {
      types = [ "REGIONAL" ]
    }
}

resource "aws_api_gateway_deployment" "lewdapi" {
  rest_api_id = aws_api_gateway_rest_api.lewdapi.id
  stage_name = "prod"

  # TODO: list out a bunch of sensitve things here that trigger a new
  # api snapshot. Those things would go in the jsonecode part as a list
  # of stuff
  triggers = {
    redeployment = sha1(jsonencode([
      aws_api_gateway_rest_api.lewdapi.body
    ]))
  }

  depends_on = [
    aws_api_gateway_integration.list_integration,
  ]
}


# Usage plan for the API key
############################


resource "aws_api_gateway_usage_plan" "usage_plan" {
  name = "Lewd Lad Prod API Key Usage Plan"
  api_stages {
    api_id = aws_api_gateway_rest_api.lewdapi.id
    stage = aws_api_gateway_deployment.lewdapi.stage_name
  }
  quota_settings {
    limit = 100
    period = "DAY"
  }
  throttle_settings {
    burst_limit = 10
    rate_limit = 10
  }
}

resource "aws_api_gateway_api_key" "apikey" {
  name = "lewdlad-api-key"
}

resource "aws_api_gateway_usage_plan_key" "apikey" {
  key_id = aws_api_gateway_api_key.apikey.id
  key_type = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.usage_plan.id
}




