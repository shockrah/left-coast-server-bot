variable "role_prefix" {
  type = string
  default = "lewd-lad-lambda"
}
######################## AWS PROVIDER CONFIG ######################## 

variable "aws_key" {
  description = "API Key for AWS"
  type = string
  sensitive = true
}
variable "aws_secret" {
  description = "AWS secret key"
  type = string
  sensitive = true
}
variable "aws_account_id"  {
  description = "Admin account ID(Not the root/May also be the project-owner account ID)"
  type = string
  sensitive = true
}


variable "milk_bucket_region" {
  description = "Milk Bucket VPC Region"
  type = string
  sensitive = true
}

variable "manage_instances_role" {
  description = "Used for lambda functions to start/stop/list instances"
  type = string
  sensitive = true
}

variable "list_path" {
  description = "URI Path of list resource"
  type = string
  default = "list"
}

variable "start_path" {
  description = "URI Path of the start resource"
  type = string
  default = "start"
}

variable "stop_path" {
  description = "URI Path of the stop resource"
  type = string
  default = "stop"
}

variable "target_vpc" {
  description = "VPC to target in lambda api"
  type = string
  sensitive = true
}

variable "beehive_id" {
  description = "Instance ID for bee-hive"
  type = string
  sensitive = true
}
