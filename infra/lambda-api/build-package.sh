#!/bin/bash
set -e
pushd src/
	if [ ! -d package ];then
		pip install --target ./package boto3
	fi
	pushd package/
		zip -qr ../package.zip .
	popd
	zip -g package.zip up.py down.py list.py state.py
popd
