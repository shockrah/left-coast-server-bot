# This module covers the resources required to create the GET /servers endpoint
# for lewd-lad's API

# API Gateway Endpoint /list
############################

resource "aws_api_gateway_resource" "list_res" {
  rest_api_id = aws_api_gateway_rest_api.lewdapi.id
  parent_id   = aws_api_gateway_rest_api.lewdapi.root_resource_id
  path_part   = var.list_path
}

resource "aws_api_gateway_request_validator" "list_request_validation" {
  name = "list-request-validation"
  rest_api_id = aws_api_gateway_rest_api.lewdapi.id
  validate_request_body       = true
  validate_request_parameters = true
}

resource "aws_api_gateway_method" "get_list" {
  authorization = "NONE"
  http_method   = "GET"
  resource_id   = aws_api_gateway_resource.list_res.id
  rest_api_id   = aws_api_gateway_rest_api.lewdapi.id
  api_key_required = true
  request_validator_id = aws_api_gateway_request_validator.request_validation.id
}


# Lambda
########

resource "aws_lambda_function" "list" {
    function_name = "list-instances"
    handler = "list.handler"
    runtime = "python3.8"
    filename = "src/package.zip"
    role = var.manage_instances_role
    environment {
      variables = {
        TARGET_VPC = var.target_vpc
      }
    }
    tags = {
      Description = "List EC2 instances in the target VPC"
    }
}

# Integrations
##############

resource "aws_api_gateway_integration" "list_integration" {
  rest_api_id = aws_api_gateway_rest_api.lewdapi.id
  resource_id = aws_api_gateway_method.get_list.resource_id

  http_method = aws_api_gateway_method.get_list.http_method
  integration_http_method = "POST"

  type = "AWS"
  uri  = aws_lambda_function.list.invoke_arn
}

# Response Configuration
########################

resource "aws_api_gateway_method_response" "list_response" {
  rest_api_id = aws_api_gateway_rest_api.lewdapi.id
  resource_id = aws_api_gateway_resource.list_res.id
  http_method = aws_api_gateway_method.get_list.http_method
  status_code = "200"
  response_models = {
    "application/json" = "Empty"
  }
}

resource "aws_api_gateway_integration_response" "list" {
  rest_api_id = aws_api_gateway_rest_api.lewdapi.id
  resource_id = aws_api_gateway_resource.list_res.id
  http_method = aws_api_gateway_method.get_list.http_method
  status_code = aws_api_gateway_method_response.list_response.status_code
  depends_on = [
    aws_api_gateway_integration.list_integration
  ]
}




