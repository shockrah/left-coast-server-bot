output "base_url" {
  value = "${aws_api_gateway_deployment.lewdapi.invoke_url}/"
  sensitive = true
}


output "api_key" {
  value = "${aws_api_gateway_api_key.apikey.value}"
  sensitive = true
}


