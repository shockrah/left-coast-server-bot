#!/bin/bash

set -e

source secrets/set-env.sh
for func in stop start list; do
	echo Reapplying $func
    terraform apply -auto-approve -replace aws_lambda_function.$func
done

terraform apply -auto-approve -replace aws_lambda_permission.allow_integration 
